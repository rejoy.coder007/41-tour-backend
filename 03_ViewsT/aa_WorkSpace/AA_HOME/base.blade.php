<!DOCTYPE html>
<html lang=en>

<head>


    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta http-equiv=ScreenOrientation content=autoRotate:disabled>
    <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name=description content="">
    <meta name=author content="">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



    <script defer  type="text/javascript" src="01_SCRIPTS/aa_home.js"></script>
    <link rel="shortcut icon" type=image/png href="http://wayanadtoursandtravels.com/02_IMAGES/favicon.png">
    <title>@yield('title')</title>
    <style>  @include('01_CSS.aa_home') </style>

</head>

<body>

@yield('content')


</body>

</html>