module.exports = {
    "env": {
        "es6": true,
        "browser": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "no-console": "off",
        "no-constant-condition": "off",
        "no-unused-vars":"off",
        "no-undef":"off",
        "quotes": [1, "double", "avoid-escape"],
        "semi": [2, "always"],
        "new-cap": 1,
        "strict": 2,

        "no-underscore-dangle": 0,
        "no-new": 0,
        "camelcase": 0

    }

};