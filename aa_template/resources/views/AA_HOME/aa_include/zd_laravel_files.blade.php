<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">



{{--
<title>{{ config('app.name', 'Laravel') }}</title>
<!-- Fonts -->
<link href="https://fonts.gstatic.com" rel="dns-prefetch">
<link type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet">
 --}}
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>



<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
