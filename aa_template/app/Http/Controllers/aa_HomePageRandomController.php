<?php

namespace App\Http\Controllers;

use App\aa_ProductTour;
use Illuminate\Http\Request;

class aa_HomePageRandomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $random_products = aa_ProductTour::inRandomOrder()->take(8)->get();
        dd($random_products);
    }


}
