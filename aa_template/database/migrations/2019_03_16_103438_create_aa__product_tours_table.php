<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAaProductToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aa__product_tours', function (Blueprint $table)
        {


            $table->increments('id');
            $table->string('package_name')->unique();
            $table->string('duration');
            $table->string('slug');
            $table->string('details')->nullable();
            $table->integer('per_head_price');
            $table->text('description');

            //       $table->integer('category_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aa__product_tours');
    }
}
