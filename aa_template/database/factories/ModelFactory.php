<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'email_verified_at' => $faker->dateTimeBetween(),
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\aa_ProductTour::class, function (Faker\Generator $faker) {

    $random_int = aa_TourProductSeeder::$count++;

    $input = array("Kuruva Package", "Pookode Package", "Bansura Package", "All wayanad Package");

    $rand_item= array_rand($input, 1);

    $item_name = $input[$rand_item]." ".$random_int;

    // 'category_id' => $category_id
    //

    /*
    $category_id =0;

    switch ($rand_item)
    {
        case "Laptop":
            $category_id=0;
            break;
        case "Desktop":
            $category_id=1;
            break;
        case "Printer":
            $category_id=2;
            break;
        case "Webcam":
            $category_id=3;
            break;

        case "Hardisk":
            $category_id=4;
            break;
    }

    $table->increments('id');
    $table->string('package_name');
    $table->string('Duration');
    $table->string('slug');
    $table->string('details')->nullable();
    $table->integer('per_head_price');
    $table->text('description');
    //       $table->integer('category_id');
    $table->timestamps();
    */

    return [
        'package_name' => $item_name,
        'slug' => str_slug($item_name, "-"),
        'Duration' => random_int(10,30),
        'details' => $faker->word,
        'per_head_price' => random_int(2000,5000),
        'description' => $faker->text,

    ];


});

